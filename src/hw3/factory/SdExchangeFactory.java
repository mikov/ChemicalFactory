package hw3.factory;

public class SdExchangeFactory implements ExchangeFactory {
    @Override
    public LiquidDetergentForWashingPlanProvider createLiquidDetergentForWashingPlanProvider() {
        return new SdLiquidDetergentForWashingPlanProvider();
    }

    @Override
    public WashingPowderPlanProvider createWashingPowderPlanProvider() {
        return new SdWashingPowderPlanProvider();
    }
}
