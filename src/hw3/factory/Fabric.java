package hw3.factory;

public class Fabric {
    public static final String SD = "synthetic detergents";
    public static final String LD = "liquid detergents";

    public ExchangeFactory createFactory(String name)throws Exception{
        switch (name){
            case SD:
                return new SdExchangeFactory();
            case LD:
                return null;
        }
        return null;
    }

    private static Fabric fabric = new Fabric();

    public static Fabric getFabric(){
        return fabric;
    }

    private Fabric(){

    }
}
