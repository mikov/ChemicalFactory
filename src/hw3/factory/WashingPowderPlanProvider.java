package hw3.factory;

import main.WashingPowderPlan;

public interface WashingPowderPlanProvider {
    WashingPowderPlan getWashingPowderPlan(int quantity);
}
