package hw3.factory;

import main.LiquidDetergentForWashingPlan;

public interface LiquidDetergentForWashingPlanProvider {
    LiquidDetergentForWashingPlan getLiquidDetergentForWashingPlan(int quantity);
}
