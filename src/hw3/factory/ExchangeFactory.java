package hw3.factory;

public interface ExchangeFactory {
    LiquidDetergentForWashingPlanProvider createLiquidDetergentForWashingPlanProvider();
    WashingPowderPlanProvider createWashingPowderPlanProvider();
}
