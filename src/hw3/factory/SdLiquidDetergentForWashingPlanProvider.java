package hw3.factory;

import main.LiquidDetergentForWashingPlan;

public class SdLiquidDetergentForWashingPlanProvider implements LiquidDetergentForWashingPlanProvider {

    @Override
    public LiquidDetergentForWashingPlan getLiquidDetergentForWashingPlan(int id) {
        return new LiquidDetergentForWashingPlan();
    }
}
