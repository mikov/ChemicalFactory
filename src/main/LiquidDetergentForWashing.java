package main;

public class LiquidDetergentForWashing extends Product {
    int volume;

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
