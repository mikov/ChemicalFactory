package main;


import java.sql.Date;

public class ProductionPlan {
    int idProductionPlan;
    Date date;
    WashingPowderPlan washingPowderPlan;
    LiquidDetergentForWashingPlan liquidDetergentForWashingPlan;

    public ProductionPlan() {

    }

    public ProductionPlan(LiquidDetergentForWashingPlan liquidDetergentForWashingPlan, WashingPowderPlan washingPowderPlan) {
    }

    public void saveToDB() {

    }

    public void setIdProductionPlan(int idProductionPlan) {
        this.idProductionPlan = idProductionPlan;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setWashingPowderPlan(WashingPowderPlan washingPowderPlan) {
        this.washingPowderPlan = washingPowderPlan;
    }

    public void setLiquidDetergentForWashingPlan(LiquidDetergentForWashingPlan liquidDetergentForWashingPlan) {
        this.liquidDetergentForWashingPlan = liquidDetergentForWashingPlan;
    }

    public Date getDate() {
        return date;
    }

    public int getIdProductionPlan() {

        return idProductionPlan;
    }
}
