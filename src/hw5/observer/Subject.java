package hw5.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private List<Observer> observerList = new ArrayList<>();

    public void attach(Observer observer) {
        observerList.add(observer);
    }

    public void detach(Observer observer) {
        observerList.remove(observer);
    }

    protected void notify(Object arg){
        for (Observer observer : observerList) {
            observer.update(this,arg);
        }
    }
}
