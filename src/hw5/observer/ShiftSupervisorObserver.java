package hw5.observer;

public class ShiftSupervisorObserver implements Observer {

    @Override
    public void update(Subject subject, Object arg) {
        int made = 0;

        for (int i = 0;i<8 ;i++ ) {
            made += 150;
            if (made > 1000) {
                System.out.printf("%s made %s\n", subject.toString(), arg);
                break;
            }
        }
    }
}
