package hw5.observer;

public class PackingLine extends Subject{
    String name;
    int needToDo;

    public PackingLine(String name) {
        this.name = name;
    }

    public int getNeedToDo() {
        return needToDo;
    }

    public void setneedToDo(int needToDo) {
        this.needToDo = needToDo;
        notify(needToDo);
    }

    public String getName() {
        return name;
    }
}
