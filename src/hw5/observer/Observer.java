package hw5.observer;

public interface Observer {
    void update(Subject subject, Object arg);
}
