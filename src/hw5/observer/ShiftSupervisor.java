package hw5.observer;

import hw3.factory.ExchangeFactory;
import hw3.factory.Fabric;
import hw3.factory.LiquidDetergentForWashingPlanProvider;
import hw3.factory.WashingPowderPlanProvider;
import main.ProductionPlan;

import java.util.Date;

public class ShiftSupervisor {
    int idShiftSupervisor;
    String name;
    String sirName;

    public static void main(String[] args) {
        PackingLine packingLine1 = new PackingLine("ACMA1");
        packingLine1.attach(new ShiftSupervisorObserver());
        packingLine1.setneedToDo(1000);
    }

    public ProductionPlan createProductionPlan(String id, Date date) throws Exception {
        ExchangeFactory exchangeFactory = Fabric.getFabric().createFactory(id);

        LiquidDetergentForWashingPlanProvider liquidDetergentForWashingPlanProvider = exchangeFactory.createLiquidDetergentForWashingPlanProvider();
        WashingPowderPlanProvider washingPowderPlanProvider = exchangeFactory.createWashingPowderPlanProvider();
        return new ProductionPlan(
                liquidDetergentForWashingPlanProvider.getLiquidDetergentForWashingPlan(2000),
                washingPowderPlanProvider.getWashingPowderPlan(1000)
        );
    }
}
