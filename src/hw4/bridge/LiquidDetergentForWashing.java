package hw4.bridge;

public class LiquidDetergentForWashing extends Product {
    int volume;

    public LiquidDetergentForWashing(State state) {
        super(state);
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public void setState() {
        state.setState();
    }
}
