package hw4.bridge;

public abstract class Product {
    int idProduct;
    String name;
    State state;

    public Product(State s) {
        this.state = s;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void setState();
}
