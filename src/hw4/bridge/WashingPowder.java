package hw4.bridge;

import main.Product;

public class WashingPowder extends hw4.bridge.Product {
    int weight;

    public WashingPowder(State state) {
        super(state);
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public void setState() {
        state.setState();
    }
}
