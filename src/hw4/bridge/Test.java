package hw4.bridge;

public class Test {
    public static void main(String[] args) {

        WashingPowder aist = new WashingPowder(new Powder());
        aist.setState();

        LiquidDetergentForWashing bos = new LiquidDetergentForWashing(new Liquid());
        bos.setState();

    }
}
