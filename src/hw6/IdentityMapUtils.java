package hw6;

import main.ProductionPlan;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class IdentityMapUtils {
    private static Map<Date,ProductionPlan> productionPlanMap = new HashMap<>();

    public static void addProductionPlan(ProductionPlan productionPlan){
        productionPlanMap.put(productionPlan.getDate(), productionPlan);
    }

    public  static ProductionPlan getProductionPlan(Date date){
        return productionPlanMap.get(date);
    }

    public static ProductionPlan getProductionPlan(Long key){
        return getProductionPlan(new Date(key));
    }
}
