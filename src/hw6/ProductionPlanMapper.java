package hw6;

import main.LiquidDetergentForWashingPlan;
import main.ProductionPlan;
import main.WashingPowderPlan;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import static hw6.IdentityMapUtils.addProductionPlan;


public class ProductionPlanMapper {
    private final Connection connection;

    public ProductionPlanMapper(Connection connection) {
        this.connection = connection;
    }

    public ProductionPlan findByDate(Date date) throws SQLException {
        ProductionPlan productionPlan = new ProductionPlan();
        PreparedStatement statement = connection.prepareStatement(
                "SELECT idProductionPlan,productionPlan FROM PLAN WHERE DATE = ?");
        statement.setDate(1, date);
        try {
            ResultSet resultSet = statement.executeQuery(); {
                while (resultSet.next()) {
                    productionPlan.setIdProductionPlan(resultSet.getInt(1));
                    productionPlan.setDate(resultSet.getDate(2));
                    productionPlan.setWashingPowderPlan((WashingPowderPlan) resultSet.getObject(3));
                    productionPlan.setLiquidDetergentForWashingPlan((LiquidDetergentForWashingPlan) resultSet.getObject(4));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productionPlan;
    }

    public ProductionPlan finder(Date date) throws SQLException {
        ProductionPlan productionPlan = IdentityMapUtils.getProductionPlan(date);
        if(productionPlan == null){
            findByDate(date);
            addProductionPlan(productionPlan);
        }
        return productionPlan;
    }
}
